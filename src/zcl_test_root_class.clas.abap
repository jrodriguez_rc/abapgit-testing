"! <p class="shorttext synchronized" lang="en">Root class</p>
CLASS zcl_test_root_class DEFINITION
  PUBLIC
  ABSTRACT
  CREATE PUBLIC .

  PUBLIC SECTION.

    "! <p class="shorttext synchronized" lang="en">Test constant</p>
    CONSTANTS co_test TYPE string VALUE 'TESTING' ##NO_TEXT.

    METHODS public .
  PROTECTED SECTION.
    METHODS protected.

    METHODS example_01
      RETURNING VALUE(rv_value) TYPE string .
    METHODS example_02 .

  PRIVATE SECTION.
    METHODS private.

ENDCLASS.



CLASS zcl_test_root_class IMPLEMENTATION.


  METHOD example_01.
    rv_value = 'Test value 01'(001).
  ENDMETHOD.


  METHOD example_02.

  ENDMETHOD.


  METHOD private.

  ENDMETHOD.


  METHOD protected.

  ENDMETHOD.


  METHOD public.

  ENDMETHOD.
ENDCLASS.
