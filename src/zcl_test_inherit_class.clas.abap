CLASS zcl_test_inherit_class DEFINITION
  PUBLIC
  INHERITING FROM zcl_test_root_class
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS public
        REDEFINITION.

  PROTECTED SECTION.
    METHODS protected
        REDEFINITION.

    METHODS example_01
        REDEFINITION .
  PRIVATE SECTION.
ENDCLASS.


CLASS zcl_test_inherit_class IMPLEMENTATION.

  METHOD example_01.

  ENDMETHOD.


  METHOD protected.

  ENDMETHOD.


  METHOD public.

  ENDMETHOD.

ENDCLASS.
